CREATE TABLE [dbo].[User] (
    [UserId]      INT IDENTITY(1,1)            NOT NULL,
    [Username]        VARCHAR (50)   NOT NULL,
    [Password]        VARCHAR (90)   NOT NULL,
    [Email]           VARCHAR (80)   NOT NULL,
    [FirstName]       VARCHAR (50)   NOT NULL,
    [LastName]        VARCHAR (50)   NOT NULL,
    [Phone]           NVARCHAR (MAX) NOT NULL,
    [ShippingAddress] VARCHAR (50)   NOT NULL,
    [ShippingCity]    VARCHAR (50)   NOT NULL,
    [ShippingState]   VARCHAR (50)   NOT NULL,
    [ShippingZIP]     VARCHAR (11)   NOT NULL,
    [ManagerStatus]   VARCHAR (11)   NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [CK_User_UNIQUE] UNIQUE NONCLUSTERED ([UserId] ASC, [Username] ASC, [Email] ASC)
);