﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Add_Inventory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ManagerStatus"] == null)
        {
            Response.Redirect("~/Unauthorized.aspx");
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string productname;
        string sku;
        string shortdesc;
        string longdesc;
        decimal price;
        decimal cost;
        string filename;
        Int32 quantity;
        Int32 category;
        DateTime date = DateTime.Now;


        productname = txtProductName.Text;
        sku = txtSKU.Text;
        shortdesc = txtShortDesc.Text;
        longdesc = txtLongDesc.Text;
        price = decimal.Parse(txtPrice.Text);
        cost = decimal.Parse(txtCost.Text);
        filename = txtFileName.Text;
        quantity = int.Parse(txtQuantity.Text);
        category = int.Parse(txtCategory.Text);


        using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security = True"))
        {
            String query = "INSERT INTO [Inventory] (ProductName, SKU, ShortDesc, LongDesc, Price, Cost, FileName, LastActivity, Quantity, Category) VALUES(@productname, @sku, @shortdesc, @longdesc, @price, @cost, @filename, @lastactivity, @quantity, @category)";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                cmd.Parameters.AddWithValue("@productname", productname);
                cmd.Parameters.AddWithValue("@sku", sku);
                cmd.Parameters.AddWithValue("@shortdesc", shortdesc);
                cmd.Parameters.AddWithValue("@longdesc", longdesc);
                cmd.Parameters.AddWithValue("@price", price);
                cmd.Parameters.AddWithValue("@cost", cost);
                cmd.Parameters.AddWithValue("@filename", filename);
                cmd.Parameters.AddWithValue("@quantity", quantity);
                cmd.Parameters.AddWithValue("@category", category);
                cmd.Parameters.AddWithValue("@lastactivity", date);


                con.Open();
                int result = cmd.ExecuteNonQuery();
                con.Close();

                // Check Error
                if (result < 0)
                    Console.WriteLine("Error inserting data into Database!");
            }
        }




        Response.Redirect("/Inventory_Manager.aspx");

    }
}