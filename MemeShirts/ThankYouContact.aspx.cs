﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ThankYouContact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Cust"] != null)
        {
            Customer aCustomer = (Customer)Session["Cust"];

            string first = aCustomer.FirstName + " " + aCustomer.LastName;

            lblName.Text = first;
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}