﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="inventory_manager.aspx.cs" Inherits="inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>

        <asp:Button ID="btnAddInventory" runat="server" Text="Add Inventory Item" OnClick="btnAddInventory_Click" />
        <asp:Button ID="btnUploadImage" runat="server" OnClick="btnUploadImage_Click" Text="Upload Image" />
        <br /><br />

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
            <asp:BoundField DataField="SKU" HeaderText="SKU" SortExpression="SKU" />
            <asp:BoundField DataField="ShortDesc" HeaderText="ShortDesc" SortExpression="ShortDesc" />
            <asp:BoundField DataField="LongDesc" HeaderText="LongDesc" SortExpression="LongDesc" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
            <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName" />
            <asp:BoundField DataField="LastActivity" HeaderText="LastActivity" SortExpression="LastActivity" />
            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
            <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Id], [ProductName], [SKU], [ShortDesc], [LongDesc], [Price], [Cost], [FileName], [LastActivity], [Quantity], [Category] FROM [Inventory]" DeleteCommand="DELETE FROM [Inventory] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Inventory] ([Id], [ProductName], [SKU], [ShortDesc], [LongDesc], [Price], [Cost], [FileName], [LastActivity], [Quantity], [Category]) VALUES (@Id, @ProductName, @SKU, @ShortDesc, @LongDesc, @Price, @Cost, @FileName, @LastActivity, @Quantity, @Category)" UpdateCommand="UPDATE [Inventory] SET [ProductName] = @ProductName, [SKU] = @SKU, [ShortDesc] = @ShortDesc, [LongDesc] = @LongDesc, [Price] = @Price, [Cost] = @Cost, [FileName] = @FileName, [LastActivity] = @LastActivity, [Quantity] = @Quantity, [Category] = @Category WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="ProductName" Type="String" />
            <asp:Parameter Name="SKU" Type="String" />
            <asp:Parameter Name="ShortDesc" Type="String" />
            <asp:Parameter Name="LongDesc" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Cost" Type="Decimal" />
            <asp:Parameter Name="FileName" Type="String" />
            <asp:Parameter DbType="Date" Name="LastActivity" />
            <asp:Parameter Name="Quantity" Type="Int32" />
            <asp:Parameter Name="Category" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ProductName" Type="String" />
            <asp:Parameter Name="SKU" Type="String" />
            <asp:Parameter Name="ShortDesc" Type="String" />
            <asp:Parameter Name="LongDesc" Type="String" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="Cost" Type="Decimal" />
            <asp:Parameter Name="FileName" Type="String" />
            <asp:Parameter DbType="Date" Name="LastActivity" />
            <asp:Parameter Name="Quantity" Type="Int32" />
            <asp:Parameter Name="Category" Type="Int32" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
        </asp:SqlDataSource>
</div>    
</asp:Content>

