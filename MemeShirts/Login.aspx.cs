﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        Session.Abandon();
        if (!Page.IsPostBack) // when page loads first time
        {
            Session["CurrentUser"] = null;
            Session.Remove("CurrentUser");
            Session.Clear();
        }
    }


    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        string username = txtUsername.Text;
        string password = txtPassword.Text;


        using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security = True"))
        {
            String query = "SELECT COUNT(*) FROM [User] WHERE [Username] = @username AND [Password] = @password";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);

                con.Open();
                //Will return 1 if the username is correct
                int result = (int)cmd.ExecuteScalar();
                con.Close();

                if (result == 1)
                {
                    Session["CurrentUser"] = username;

                }
                else
                {
                    Session["CurrentUser"] = null;
                    lblError.Text = "Incorrect Login Credentials. Please try again.";
                    Response.Redirect("~/LogInError.aspx");

                }
            }
        }



        Response.Redirect("~/Default.aspx");
    }

    protected void btnChangePass_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/ChangePassword.aspx");
    }
}