﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ManagerStatus"] == null)
        {
            Response.Redirect("~/Unauthorized.aspx");
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fuUpload.HasFile)
        {
            if ((fuUpload.PostedFile.ContentType == "image/jpeg") ||
                (fuUpload.PostedFile.ContentType == "image/png") ||
                (fuUpload.PostedFile.ContentType == "image/bmp") ||
                (fuUpload.PostedFile.ContentType == "image/gif"))
            {


                if (Convert.ToInt64(fuUpload.PostedFile.ContentLength) < 50000000)
                {
                    string photoFolder = Server.MapPath("~/Images/" + User.Identity.Name);
                    //string photoFolder = Path.Combine(@"~\Images\", User.Identity.Name); //

                    if (!Directory.Exists(photoFolder))
                        Directory.CreateDirectory(photoFolder);

                    // sample.jpg
                    string extension = Path.GetExtension(fuUpload.FileName); //.jpg
                    string uniqueFileName = Path.ChangeExtension(fuUpload.FileName, DateTime.Now.Ticks.ToString());
                    // sample.12345678.jpg

                    fuUpload.SaveAs(Path.Combine(photoFolder, uniqueFileName + extension));

                    lblStatus.Text = "<font color='Green'>Succesfully uploaded " + fuUpload.FileName + "</font>";

                    //Image1.ImageUrl = photoFolder + uniqueFileName + extension;
                    //Image1.ImageUrl = "~/Images/IMG_2729.636480782022397611.PNG";
                    Image1.ImageUrl = "~/Images/" + uniqueFileName + extension;


                }
                else
                    lblStatus.Text = "File must be less than 10 MB.";
            }
            else
                lblStatus.Text = "File must be of type jpeg, jpg, png, bmp, or gif.";
        }
        else
            lblStatus.Text = "No file selected."; 
                        
        
    }
}