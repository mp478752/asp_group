﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Windows;
using System.Data;

public partial class ChangePassword : System.Web.UI.Page
{
    SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string username = txtUsername.Text;
        string oldpassword = txtOldPassword.Text;
        string newpassword = txtPassword.Text;
        string renter = txtRenter.Text;



        string _query = "UPDATE [User] set Password = @new  WHERE Username = @user AND Password = @old";

        using (SqlCommand comm = new SqlCommand())
        {
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = _query;
            comm.Parameters.AddWithValue("@user", username);
            comm.Parameters.AddWithValue("@old", oldpassword);
            comm.Parameters.AddWithValue("@new", newpassword);


            try
            {
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('No Success?');</script>");
            }
        }

        Response.Redirect("~/PasswordChanged.aspx");
    }
}