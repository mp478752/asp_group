﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ThankYouSignUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Cust"] != null)
        {
            Customer aCust = (Customer)Session["Cust"];
            //string first = Session["Cust"].ToString();
            string first = aCust.FirstName + " " + aCust.LastName;

            lblName.Text = first;
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void txtLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Login.ASPX");
    }
}