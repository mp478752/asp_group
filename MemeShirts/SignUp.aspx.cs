﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class SignUp : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string username;
        string first;
        string last;
        string email;
        string phone;
        string address;
        string city;
        string state;
        string zip;
        string password;

        username = txtUsername.Text;
        first = txtFirstName.Text;
        last = txtLastName.Text;
        email = txtEmail.Text;
        phone = txtPhone.Text;
        address = txtAddress.Text;
        city = txtCity.Text;
        state = drpState.Text;
        zip = txtZip.Text;
        password = txtPassword.Text;


        Customer aCust = new Customer(username, first, last, email, phone, address, city, state, zip, password);

        Session["Cust"] = aCust;


        using (SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security = True"))
        {
            String query = "INSERT INTO [User] (Username, Password, Email, FirstName, LastName, Phone, ShippingAddress, ShippingCity, ShippingState, ShippingZIP) VALUES(@username, @password, @email, @first, @last,  @phone, @address, @city, @state, @zip)";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                //cmd.Parameters.AddWithValue("@id", "abc");
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@first", first);
                cmd.Parameters.AddWithValue("@last", last);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@phone", phone);
                cmd.Parameters.AddWithValue("@address", address);
                cmd.Parameters.AddWithValue("@city", city);
                cmd.Parameters.AddWithValue("@state", state);
                cmd.Parameters.AddWithValue("@zip", zip);
                cmd.Parameters.AddWithValue("@password", password);
                

                con.Open();
                int result = cmd.ExecuteNonQuery();

                // Check Error
                if (result < 0)
                    Console.WriteLine("Error inserting data into Database!");
            }
        }




        Response.Redirect("/ThankYouSignUp.ASPX");
    }
}