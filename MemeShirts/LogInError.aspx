﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LogInError.aspx.cs" Inherits="LogInError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Log In Error</h1>
    <p>There was an issue while trying to log in. Please try again.</p>
    <p>
        <asp:Button ID="btnLogInPage" runat="server" OnClick="btnLogInPage_Click" Text="Log In" />
    </p>
</asp:Content>

