﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class inventory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ( Session["ManagerStatus"] == null)
        {
            Response.Redirect("~/Unauthorized.aspx");
        }
    }


    protected void btnAddInventory_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Add_Inventory.aspx");
    }

    protected void btnUploadImage_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Upload.aspx");
    }
}