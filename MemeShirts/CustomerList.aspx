﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="CustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Customer List</h1>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="UserId" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="UserId" HeaderText="Id" SortExpression="UserId" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                <asp:BoundField DataField="ShippingAddress" HeaderText="ShippingAddress" SortExpression="ShippingAddress" />
                <asp:BoundField DataField="ShippingCity" HeaderText="ShippingCity" SortExpression="ShippingCity" />
                <asp:BoundField DataField="ShippingState" HeaderText="ShippingState" SortExpression="ShippingState" />
                <asp:BoundField DataField="ShippingZIP" HeaderText="ShippingZIP" SortExpression="ShippingZIP" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Username], [Email], [FirstName], [LastName], [Phone], [ShippingAddress], [ShippingCity], [ShippingState], [ShippingZIP], [UserId] FROM [User] WHERE ([ManagerStatus] IS NULL)"></asp:SqlDataSource>
    </p>
</asp:Content>

