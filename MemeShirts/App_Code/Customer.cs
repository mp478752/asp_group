﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Customer
/// </summary>
public class Customer
{

    public string Username { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string ShippingAddress { get; set; }
    public string ShippingCity { get; set; }
    public string ShippingState { get; set; }
    public string ShippingZIP { get; set; }
    public string Password { get; set; }

    public string Comment { get; set; }

    public Customer()
    {
    }


    public Customer(string username, string first, string last, string email, string phone, string address, string city, string state, string zip, string password)
    {
        Username = username;
        FirstName = first;
        LastName = last;
        Email = email;
        Phone = phone;
        ShippingAddress = address;
        ShippingCity = city;
        ShippingState = state;
        ShippingZIP = zip;
        Password = password;
    }

    public Customer(string first, string last, string email, string comment)
    {
        FirstName = first;
        LastName = last;
        Comment = comment;
    }

}