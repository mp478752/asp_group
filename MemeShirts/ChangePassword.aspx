﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 196px;
        }
        .auto-style2 {
            width: 234px;
        }
        .auto-style3 {
            width: 163px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <Table ID="Table1" >
        <tr>
            <td class="auto-style3">Username</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtUsername" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsername" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Old Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtOldPassword" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtOldPassword" ErrorMessage="Required"></asp:RequiredFieldValidator>
&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">New Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPassword" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Renter New Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtRenter" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRenter" ErrorMessage="Required"></asp:RequiredFieldValidator>
&nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtRenter" ErrorMessage="Password does not match"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="110px" OnClick="btnSubmit_Click" />
            </td>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2"> &nbsp;</td>
        </tr>
    </Table>
</asp:Content>

