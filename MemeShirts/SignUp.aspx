﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 188px;
        }
        .auto-style2 {
            width: 301px;
        }
        .auto-style3 {
            width: 123px;
        }
        .auto-style4 {
            width: 123px;
            height: 26px;
        }
        .auto-style5 {
            width: 188px;
            height: 26px;
        }
        .auto-style6 {
            width: 301px;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <Table ID="Table1" >
        <tr>
            <td class="auto-style3">Username</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtUsername" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsername" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

       <tr>
                    <td class="auto-style3">First Name</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtFirstName" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>
        </tr>
        <tr>
                    <td class="auto-style3">Last Name</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtLastName" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLastName" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>

        </tr>
        <tr>
                    <td class="auto-style4">Phone</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtPhone" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style6"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPhone" ErrorMessage="Required"></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone number not in correct format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
                
            </td>

        </tr>
        <tr>
                    <td class="auto-style4">Address</td>
            <td class="auto-style5">
                <asp:TextBox ID="txtAddress" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style6"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtAddress" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>

        </tr>
        <tr>
                    <td class="auto-style3">City</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtCity" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtCity" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>

        </tr>
        <tr>
                    <td class="auto-style3">State</td>
            <td class="auto-style1">
                <asp:DropDownList ID="drpState" runat="server" Height="16px" Width="48px" MaxLength="50">
                    <asp:ListItem>AL</asp:ListItem>
                    <asp:ListItem>AK</asp:ListItem>
                    <asp:ListItem>AZ</asp:ListItem>
                    <asp:ListItem>AR</asp:ListItem>
                    <asp:ListItem>CA</asp:ListItem>
                    <asp:ListItem>CO</asp:ListItem>
                    <asp:ListItem>CT</asp:ListItem>
                    <asp:ListItem>DE</asp:ListItem>
                    <asp:ListItem>FL</asp:ListItem>
                    <asp:ListItem>GA</asp:ListItem>
                    <asp:ListItem>HI</asp:ListItem>
                    <asp:ListItem>ID</asp:ListItem>
                    <asp:ListItem>IL</asp:ListItem>
                    <asp:ListItem>IN</asp:ListItem>
                    <asp:ListItem>IA</asp:ListItem>
                    <asp:ListItem>KS</asp:ListItem>
                    <asp:ListItem>KY</asp:ListItem>
                    <asp:ListItem>LA</asp:ListItem>
                    <asp:ListItem>ME</asp:ListItem>
                    <asp:ListItem>MD</asp:ListItem>
                    <asp:ListItem>MA</asp:ListItem>
                    <asp:ListItem>MI</asp:ListItem>
                    <asp:ListItem>MN</asp:ListItem>
                    <asp:ListItem>MS</asp:ListItem>
                    <asp:ListItem>MO</asp:ListItem>
                    <asp:ListItem>MT</asp:ListItem>
                    <asp:ListItem>NE</asp:ListItem>
                    <asp:ListItem>NV</asp:ListItem>
                    <asp:ListItem>NH</asp:ListItem>
                    <asp:ListItem>NJ</asp:ListItem>
                    <asp:ListItem>NM</asp:ListItem>
                    <asp:ListItem>NY</asp:ListItem>
                    <asp:ListItem>NC</asp:ListItem>
                    <asp:ListItem>ND</asp:ListItem>
                    <asp:ListItem>OH</asp:ListItem>
                    <asp:ListItem>OK</asp:ListItem>
                    <asp:ListItem>OR</asp:ListItem>
                    <asp:ListItem>PA</asp:ListItem>
                    <asp:ListItem>RI</asp:ListItem>
                    <asp:ListItem>SC</asp:ListItem>
                    <asp:ListItem>SD</asp:ListItem>
                    <asp:ListItem>TN</asp:ListItem>
                    <asp:ListItem>TX</asp:ListItem>
                    <asp:ListItem>UT</asp:ListItem>
                    <asp:ListItem>VT</asp:ListItem>
                    <asp:ListItem>VA</asp:ListItem>
                    <asp:ListItem>WA</asp:ListItem>
                    <asp:ListItem>WV</asp:ListItem>
                    <asp:ListItem>WI</asp:ListItem>
                    <asp:ListItem>WY</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="auto-style2"> 
                
                &nbsp;</td>
            
        </tr>
        <tr>
                    <td class="auto-style3">Zip</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtZip" runat="server" Width="173px" MaxLength="11"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtZip" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip not formated correctly" ValidationExpression="\d{5}(-\d{4})?"></asp:RegularExpressionValidator>
                
            </td>

        </tr>
        

        <tr>
            <td class="auto-style3">Email</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtEmail" runat="server" Width="173px" MaxLength="80"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Required"></asp:RequiredFieldValidator>
&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email not in correct format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPassword" runat="server" Width="173px" TextMode="Password" MaxLength="90"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Renter Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtRenter" runat="server" Width="173px" TextMode="Password" MaxLength="90"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRenter" ErrorMessage="Required"></asp:RequiredFieldValidator>
&nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtRenter" ErrorMessage="Password does not match"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="110px" OnClick="btnSubmit_Click" />
            </td>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2"> &nbsp;</td>
        </tr>
    </Table>

</asp:Content>

