﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style2 {
            width: 108px;
        }
        .auto-style3 {
            width: 110px;
        }
        .auto-style4 {
            width: 189px;
        }
        .auto-style5 {
            width: 458px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <Table ID="Table1" class="auto-style5" >
        <tr>
            <td class="auto-style3">Username</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtUsername" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                &nbsp; 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsername" ErrorMessage="Required" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>


        <tr>
            <td class="auto-style3">Password</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtPassword" runat="server" Width="173px" TextMode="Password"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                &nbsp; 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword" ErrorMessage="Required" ValidationGroup="1"></asp:RequiredFieldValidator>
&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">
                
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click1" Text="Login" Width="106px" ValidationGroup="1" />
            </td>
            <td class="auto-style4">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <td class="auto-style2"> 
                <asp:Button ID="btnChangePass" runat="server" OnClick="btnChangePass_Click" Text="Change Password" Width="143px" />
            </td>
        </tr>
    </Table>
</asp:Content>

