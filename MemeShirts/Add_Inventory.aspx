﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Add_Inventory.aspx.cs" Inherits="Add_Inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
            width: 196px;
        }
        .auto-style2 {
            width: 329px;
        }
        .auto-style3 {
            height: 26px;
            width: 164px;
        }
        .auto-style4 {
            height: 26px;
            width: 329px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <Table ID="Table1" >
        
       <tr>
                    <td class="auto-style3">Product Name</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtProductName" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtProductName" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>
        </tr>
        <tr>
                    <td class="auto-style3">SKU</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtSKU" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style4"> 
                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtSKU" ErrorMessage="Required"></asp:RequiredFieldValidator>
                
            </td>

      
        <tr>
            <td class="auto-style3">
                Short Description</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtShortDesc" runat="server" Width="173px" MaxLength="50"></asp:TextBox>
            </td>
            <td class="auto-style4"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtShortDesc" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                Long Description</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtLongDesc" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtLongDesc" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                Price</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPrice" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtPrice" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator id="CompareValidator1" runat="server" ControlToValidate="txtPrice" Operator="DataTypeCheck" Type="Currency"  Display="Dynamic" ErrorMessage="Illegal format for currency" />
            </td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                Cost</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtCost" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtCost" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator id="CheckFormat1" runat="server" ControlToValidate="txtCost" Operator="DataTypeCheck" Type="Currency"  Display="Dynamic" ErrorMessage="Illegal format for currency" />
            </td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                FileName</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtFileName" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> &nbsp;</td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                Quantity</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtQuantity" runat="server" Width="173px"></asp:TextBox>
            </td>
            <td class="auto-style2"> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtQuantity" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtQuantity" ErrorMessage="Value must be a whole number" Display="Dynamic" />
            </td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                Category</td>
            <td class="auto-style1">
                <asp:TextBox ID="txtCategory" runat="server" ReadOnly="True" Width="173px">1</asp:TextBox>
            </td>
            <td class="auto-style2"> &nbsp;</td>
        </tr>

      
        <tr>
            <td class="auto-style3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="110px" OnClick="btnSubmit_Click" />
            </td>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2"> &nbsp;</td>
        </tr>
    </Table>
</asp:Content>

