﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Windows;
using System.Data;

public partial class qA : System.Web.UI.Page
{
    SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
    //SqlConnection con = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //SqlConnection con = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        string _query = "INSERT INTO [Questions] (Firstname,Lastname,Email,Question) values (@first,@last,@email,@question)";

        using (SqlCommand comm = new SqlCommand())
        {
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = _query;
            comm.Parameters.AddWithValue("@first", txtFName.Text);
            comm.Parameters.AddWithValue("@last", txtLName.Text);
            comm.Parameters.AddWithValue("@email", txtEmail.Text);
            comm.Parameters.AddWithValue("@question", txtQuestion.Text);

            try
            {
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('No Success?');</script>");
            }
        }

        //SqlCommand cmd = connect.CreateCommand();
        //cmd.CommandType = System.Data.CommandType.Text;
        //cmd.CommandText = "insert into customer comments("txtFirstName
        string first;
        string last;
        string email;
        string comment;

        first = txtFName.Text;
        last = txtLName.Text;
        email = txtEmail.Text;
        comment = txtQuestion.Text;


        Customer aCustomer = new Customer(first, last, email, comment);
        Session["Cust"] = aCustomer;

        Response.Redirect("~/ThankYouContact.ASPX");
    }


}
