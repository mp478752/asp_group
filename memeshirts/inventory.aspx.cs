﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class inventory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        String csname1 = "PopupScript";
        Type cstype = this.GetType();

        ClientScriptManager cs = Page.ClientScript;
        String cstext1 = "alert('this item has been added to your order');";
        cs.RegisterStartupScript(cstype, csname1, cstext1, true);

    }
}