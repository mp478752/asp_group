﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="qA.aspx.cs" Inherits="qA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style2 {
            width: 105px;
            height: 22px;
        }
        .auto-style3 {
            height: 22px;
            width: 612px;
        }
        .auto-style4 {
            width: 612px;
        }
        </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                       <table> 
                           <tr>
                               <td>First name:</td>
                               <td class="auto-style4"> 
                                   <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name is requird" ControlToValidate="txtFName" ValidationGroup="2"></asp:RequiredFieldValidator>
                               </td>
                               
                           </tr>
                           <tr> 
                               <td>Last name:</td>
                               <td class="auto-style4">
                                   <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name is required" ControlToValidate="txtLName" ValidationGroup="2"></asp:RequiredFieldValidator>
                               </td>
                           </tr>
                           <tr>
                                <td>Email:</td>
                               <td class="auto-style4">
                                   <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required" ValidationGroup="2"></asp:RequiredFieldValidator>
&nbsp;&nbsp;&nbsp;
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Email must be in correct format" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="2"></asp:RegularExpressionValidator>
                                &nbsp;&nbsp;
                                   </td>
                               
                           </tr>
                           <tr> 
                               <td class="auto-style2">
                                   Question: 
                               </td>
                               <td class="auto-style3">

                                   <asp:TextBox ID="txtQuestion" runat="server" Width="348px" Height="108px" TextMode="MultiLine"></asp:TextBox>

                               &nbsp;&nbsp;&nbsp;
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Question is required" ControlToValidate="txtQuestion" ValidationGroup="2"></asp:RequiredFieldValidator>

                               </td>
                           </tr>

                           <tr>
                               <td> 
                                   <asp:Button ID="btnSubmit" runat="server" Height="29px" OnClick="btnSubmit_Click" Text="Submit" Width="103px" ValidationGroup="2" />
                               </td>
                           </tr>
                       </table>
                       <br />
                       <asp:GridView ID="GridView1" runat="server" Width="917px" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" >
                           <Columns>
                               <asp:BoundField DataField="Question" readonly="true" HeaderText="Question" SortExpression="Question" />
                               <asp:BoundField DataField="Answer" HeaderText="Answer" SortExpression="Answer" />
                               <asp:CommandField ShowEditButton="True" />
                           </Columns>
                       </asp:GridView>
                       <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Question], [Answer] FROM [Questions]"
                           UpdateCommand="UPDATE [Questions] SET [Answer] = @Answer">
                       </asp:SqlDataSource>
                       <br />
</asp:Content>

